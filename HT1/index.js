/**
 *  ######################## CONFIGURACIÓN DEL SERVIDOR ########################
 */

//Biblioteca para el módulo express
const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());

//Constante inicial
const port = 3000;

//Biblioteca para el servidor http
const http = require('http');

//Creación del servidor
const server = http.createServer(app);

/**
 *  ######################## CONFIGURACIÓN DE LA CONEXIÓN A MONGODB ########################
 */

const MongoClient = require('mongodb').MongoClient;
const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/test';

/**
 *  ######################## RUTA ########################
 */
app.get('/', (req, res, next) => {
    //Ruta oficial para la conexión con MONGO
    MongoClient.connect(mongoUrl, 
        {
            useNewUrlParser: true
        },
        (err, db) => {
            if(err)
            {
                res.status(500).send('Pablo César Pacheco Fuentes - 201602730 - No se ha podido conectar a MongoDB con la URL: ' + mongoUrl);
            }
            else
            {
                res.send('Pablo César Pacheco Fuentes - 201602730 - Se ha conectado exitosamente a la DB con la URL: ' + mongoUrl);
                db.close();
            }
        })
});

/**
 *  ######################## SE INICIA EL SERVIDOR Y SE DEJA ESCUCHANDO ########################
 */
server.listen(port, () => {
    console.log('server on port ', port);
})